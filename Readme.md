## SuperHeros On Demand ##

A empresa **SuperHeros On Demand** tem como missão disponibilizar os melhores super herois do mercado para quem deles precisar. A empresa poderá providenciar um Super heroi 
para missões de salvamento, mas também para festas de aniversário, demonstrações em eventos publicos etc.

Para melhor fazer a gestão dos seus super heróis a empresa quere desenolver uma applicação capaz de gerir a sua atividade.

O que é um super herói

	
> "In modern popular fiction, a superhero (sometimes rendered super-hero or super hero) is a type of heroic character possessing extraordinary talents, supernatural phenomena, or superhuman powers and is dedicated to a moral goal or protecting the public."
		[en.wikipedia.org/wiki/Superhero](en.wikipedia.org/wiki/Superhero "Superhero")
        
        
		
### Esta aplicação deve ser capaz de : ###
- Gerir os vários super herois disponiveis na empresa guardando dados como:
	- Name
	- Gender
	- BirthDate
	- Email Adderess
	- Home, Work and Secret Address
	- Phone Number
	- Team Marvel, etc
	- Heroic Words
	- SuperPower
	- KnownEmemies
	- Availability
	- HourlyRate

- Gerir os clientes da empresa
	- Name
	- Gender
	- BirthDate
	- Email Adderess
	- Home, Work
	- Phone Number
	- Prefered SuperHero
	- SubscriptionInfo
- Gerir SuperHero Calls
	- Date
	- Location
	- SuperHero Requested
	- Aditional Info
	- TypeOfCall
	- State
	- Customer Ranking
	

### Requisito técnico ###
Todas as acções efetuadas no sistema devem ser registas no nosso altamente moderno e robusto sistema de logging baseado em ficheiros txt.


### Notas ###
Para escrita no ficheiro pode ser utilizado o médoto File.AppendAllLines()


[https://en.wikipedia.org/wiki/List_of_superhero_teams_and_groups](https://en.wikipedia.org/wiki/List_of_superhero_teams_and_groups "List_of_superhero_teams_and_groups")
			
			
			